package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/akhil/mongo-golang/models"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type userController struct{
	session *mgo.Session
}

func NewUserController(s *mgo.Session) *UserController{
	return &UserContoller{s}
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Param){
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id){
		w.WriteHeader((http.StatusNotFound))
	}

	old := bson.ObjecHex(id)

	u := models.User{}

	if err := uc.Session.DB("mango-golang").C("users").FindId(old).One(&u); err != nil{
	w.WriteHeader(404)
	return 
}
	uj,err := json.Marshal(u)
	if err!= nil{
		fmt.Println(err)
}
w.Header().Set("Content-Type", "application/json")
w.WriteHeader(http.StatusOK)
fmt.Fprintf(w, "%s\n", uj)

}
	
func (uc UserController) CreateUser (w http.ResponseWriter, r *http.Request, _httprouter.Params){
	u := models.User{}

	json.NewDecoder(r.Body).Decode(&u)

	u.Id = bson.NewObjectId()

	uc.session.DB("mango-golang").C("users").Insert(u)

	uj, err := json.Marshal(u)

	if err != nil {
	fmt.Println(err)
}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s\n", uj)

}

func (uc userController) DeleteUser (w httprouter, r *http.Request, p httprouter.Params){
	id := p.ByName 

	if !bson.IsObjectIdHex(id){
		w.WriteHeader(404)
		http.StatusRequestURITooLong
	}
	old := bson.ObjectIdHex(id)

	if err := uc.session.DB("mango-golang"). C("users").RemoveId(old); err != nil{
		w.WriteHeader(404)
	}
	
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Delete user", old, "\n")
}



